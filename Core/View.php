<?php
/**
 * Created by Rick Dennison
 *
 * Date: 1/6/2021
 * File: View.php
 * Name: MVC-2021
 */

namespace Core;


class View
{
    /**
     * Render a view template using Twig
     * @param string $template The template file
     * @param array $args Associative array of data to display in the view (optional)
     */
    public static function renderTemplate($template, $args = [])
    {
        static $twig = null;

        if ($twig === null) {
            $loader = new \Twig\Loader\FilesystemLoader(dirname(__DIR__) . '/App/Views');
            $twig = new \Twig\Environment($loader);
            $twig->addGlobal('current_user', \App\Auth::getUser());
            $twig->addGlobal('flash_messages', \App\Flash::getMessages());
        }
        echo $twig->render($template, $args);
    }
}