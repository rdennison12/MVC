# Model View Controller (MVC)

Description: This is a combination of three Udemy courses about building an MVC Framework.
                It will be the basis of some "How To" videos to help others.

## Commits

* Initial commit - repo and project created
* Imported folder structure into project
* Begin building the router class
* Add Base controller class
* Created Users class for later administration use
* Added the Twig template framework
* Begin testing the templates
* Begin implementing the model and configuration
* MVC framework complete
    * Next step is adding Registration and Login System
* Start building Complete Registration and Login System
* Added browser validation using JavaScript
* Added Ajax request to validate email on client side
* Removed the password conformation field and added ShowHidePassword.js
* Begin adding login functionality (e.g. email and password verification
* Redirect from Core\Controller
* Configure Sessions to remember user status
* Fixed issues with logging in and out
* Completed Sessions configuration
* Begin restricting access with authentication
* Redirect after logging in
* Completed User Authentication functionality
* Added Flash messages for the User
* Added "Remember Me" functionality to allow user to optionally auto-login
* Reinstalled the Twig framework to fix an issue 