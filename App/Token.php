<?php
/**
 * Created by Rick Dennison
 * Date:      1/19/21
 *
 * File Name: Token.php
 * Project:   MVC-2021
 */

namespace App;


use function bin2hex;
use function hash_hmac;
use function random_bytes;

class Token
{
    /**
     * Value of the token
     *
     * @var array
     */
    protected $token;

    /**
     * Token constructor.
     * @param null $token_value
     */
    public function __construct($token_value = null)
    {
        if ($token_value)
        {
            $this->token = $token_value;
        }
        else
        {
            $this->token = bin2hex(random_bytes(16)); // 16 bytes = 128 bits = 32 hex characters
        }
    }

    /**
     * @return array|string  The value
     */
    public function getValue()
    {
        return $this->token;
    }

    /**
     * @return string The hashed value
     */
    public function getHash()
    {
        return hash_hmac('sha256', $this->token, \App\Config::SECRET_KEY); // sha256 = 64 chars
    }
}