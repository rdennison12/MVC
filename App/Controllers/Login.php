<?php
/**
 * Created by Rick Dennison
 * Date:      1/17/21
 *
 * File Name: Login.php
 * Project:   MVC-2021
 */

namespace App\Controllers;


use App\Flash;
use App\Models\User;
use App\Token;
use Core\View;
use \App\Auth;
use function time;

class Login extends \Core\Controller
{
    /**
     * Show the login page
     *
     * @return void
     */
    public function loginAction()
    {
        View::renderTemplate('Login/login.html');
    }

    public function createAction()
    {
        $user = User::authenticate($_POST['email'], $_POST['password']);
        $remember_me = isset($_POST['remember_me']);

        if ($user) {
            Auth::login($user, $remember_me);

            // Remember the login here
            // TODO

            Flash::addMessage('Login Successful');

            $this->redirect(Auth::getReturnToPage());

        } else {
            Flash::addMessage('Login failed, Please try again', Flash::WARNING);

            View::renderTemplate('Login/login.html', [
                'email' => $_POST['email'],
                'remember_me' => $remember_me
            ]);
        }
    }

    /**
     * Logout the user
     *
     * @return void
     */
    public function destroyAction()
    {
        Auth::logout();

        $this->redirect('/login/show-logout-message');


    }

    public function ShowLogoutMessageAction(): void
    {
        Flash::addMessage('Logout successful');
        // Send user back to the home page
        $this->redirect('/');
    }


}