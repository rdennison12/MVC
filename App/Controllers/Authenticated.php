<?php
/**
 * Created by Rick Dennison
 * Date:      1/19/21
 *
 * File Name: Authenticated.php
 * Project:   MVC-2021
 */

namespace App\Controllers;


abstract class Authenticated extends \Core\Controller
{
    /**
     * Require the user to be authenticated before giving access to all methods in the controller
     *
     * @return void
     */
    protected function before()
    {
        $this->requireLogin();
    }
}