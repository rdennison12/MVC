<?php
/**
 * Created by Rick Dennison
 * Date:      1/18/21
 *
 * File Name: Items.php
 * Project:   MVC-2021
 */

namespace App\Controllers;

use \Core\View;

class Items extends Authenticated
{
    public function indexAction()
    {
        $this->requireLogin();

        View::renderTemplate('Items/index.html');
    }
}