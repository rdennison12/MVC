<?php
/**
 * Created by Rick Dennison
 * Date:      1/19/21
 *
 * File Name: Flash.php
 * Project:   MVC-2021
 */

namespace App;


class Flash
{
    /**
     * Alert Message Types
     * SUCCESS, INFO, and WARNING
     * @var string
     */
    const SUCCESS = 'success';
    const INFO    = 'info';
    const WARNING = 'warning';
    /**
     * Add a message
     *
     * @param string $message Message content
     *
     * @param string $type Defaults to success (Optional)
     * @return void
     */
    public static function addMessage($message, $type = 'success')
    {
        // Create an array in the session if it does not already exist
        if (!isset($_SESSION['flash_notifications']))
        {
            $_SESSION['flash_notifications'] = [];
        }
        // Append message to the array
        $_SESSION['flash_notifications'][] = [
            'body' => $message,
            'type' => $type
        ];
    }

    /**
     * Get all messages
     *
     * @return mixed  Returns an array with all the messages if set
     */
    public static function getMessages()
    {
        if (isset($_SESSION['flash_notifications']))
        {
            $messages = $_SESSION['flash_notifications'];
            unset($_SESSION['flash_notifications']);

            return $messages;
        }
    }
}